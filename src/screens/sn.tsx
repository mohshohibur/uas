import React from 'react'
import { View, Text,Image } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'

const As = () => {
    return (
        <ScrollView>
            <View style={{
                    alignItems:'center',backgroundColor:'#ffd900',height:805}}>
                    <View>
                        <Image 
                        source={require('../gambar/bsm.png')}
                        style={{height:100,width:200}}
                        />
                    </View>
                    <View style={{
                        justifyContent:'center',
                        alignItems:'center', 
                        height:60,
                        width:300, 
                        borderWidth:5,
                        borderColor:'#FFFFFF',
                        borderRadius:30,
                        marginTop:20}}>

                        <Text style={{fontSize:15,color:'black' ,fontWeight:'bold' }}>
                        SHOLAWAT NARIYAH</Text>
                    </View>

                    <View style={{
                        justifyContent:'center',
                        padding:10,
                        height:300,
                        width:360, 
                        borderWidth:2,
                        borderColor:'#FFFFFF',
                        marginTop:20}}>

                        <Text style={{fontSize:25,color:'black',marginLeft:10}}>
                        اَللّٰهُمَّ صَلِّ صَلَاةً كَامِلَةً وَسَلِّمْ سَلَامًا تَامًّا عَلىٰ سَيِّدِنَا مُحَــمَّدِ  ࣙالَّذِيْ تَنْحَلُّ بِهِ الْعُقَدُ وَتَنْفَرِجُ بِهِ الْكُرَبُ وَتُقْضٰى بِهِ الْحَوَائِجُ وَتُنَالُ
                        بِهِ الرَّغَائِبُ وَحُسْنُ الْخَوَاتِمِ وَيُسْتَسْقَى الْغَمَامُ بِوَجْهِهِ الْكَرِيْمِ وَعَلىٰ اٰلِهِ وِصَحْبِهِ فِيْ كُلِّ لَمْحَةٍ وَ نَفَسٍ بِعَدَدِ كُلِّ مَعْلُوْمٍ لَكَ

                        </Text>
                    </View>
            </View>
        </ScrollView>
    )
}

export default As
